<div class="page-header">
    <h1>Data Maintenance</h1>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <form class="form-inline">
            <input type="hidden" name="m" value="maintenance" />
            <div class="form-group">
                <input class="form-control" type="text" placeholder="Pencarian. . ." name="q" value="<?= $_GET['q'] ?>" />
            </div>
            <div class="form-group">
                <button class="btn btn-success"><span class="glyphicon glyphicon-refresh"></span> Refresh</button>
            </div>
            <div class="form-group">
                <a class="btn btn-primary" href="?m=maintenance_tambah"><span class="glyphicon glyphicon-plus"></span> Tambah</a>
            </div>
        </form>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered table-hover table-striped">
            <thead>
                <tr class="nw">
                    <th>No</th>
                    <th>Nama Pelanggan</th>
                    <th>Teknisi</th>
                    <th>Kelas</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <?php
            $q = esc_field($_GET['q']);
            $rows = $db->get_results("SELECT k.kode_maintenance, k.kode_pelanggan, k.kode_kelas, k.kode_teknisi, m.nama_pelanggan, m.sks, d.nama_teknisi	 
            FROM tb_maintenance k 
            	INNER JOIN tb_teknisi d ON d.kode_teknisi=k.kode_teknisi
            	INNER JOIN tb_pelanggan m ON m.kode_pelanggan=k.kode_pelanggan
            WHERE m.nama_pelanggan LIKE '%$q%' OR d.nama_teknisi LIKE '%$q%'
            ORDER BY k.kode_pelanggan");
            $no = 0;

            foreach ($rows as $row) : ?>
                <tr>
                    <td><?= ++$no ?></td>
                    <td><?= $row->nama_pelanggan ?></td>
                    <td><?= $row->nama_teknisi ?></td>
                    <td><?= $row->kode_kelas ?></td>
                    <td class="nw">
                        <a class="btn btn-xs btn-warning" href="?m=maintenance_ubah&ID=<?= $row->kode_maintenance ?>"><span class="glyphicon glyphicon-edit"></span></a>
                        <a class="btn btn-xs btn-danger" href="aksi.php?act=maintenance_hapus&ID=<?= $row->kode_maintenance ?>" onclick="return confirm('Hapus data?')"><span class="glyphicon glyphicon-trash"></span></a>
                    </td>
                </tr>
            <?php endforeach ?>
        </table>
    </div>
</div>