<?php
require_once 'functions.php';

/** LOGIN */
if ($act == 'login') {
    $user = esc_field($_POST['user']);
    $pass = esc_field($_POST['pass']);

    $row = $db->get_row("SELECT * FROM tb_admin WHERE user='$user' AND pass='$pass'");
    if ($row) {
        $_SESSION['login'] = $row->user;
        redirect_js("index.php");
    } else {
        print_msg("Salah kombinasi username dan password.");
    }
} else if ($mod == 'password') {
    $pass1 = $_POST['pass1'];
    $pass2 = $_POST['pass2'];
    $pass3 = $_POST['pass3'];

    $row = $db->get_row("SELECT * FROM tb_admin WHERE user='$_SESSION[login]' AND pass='$pass1'");

    if ($pass1 == '' || $pass2 == '' || $pass3 == '')
        print_msg('Field bertanda * harus diisi.');
    elseif (!$row)
        print_msg('Password lama salah.');
    elseif ($pass2 != $pass3)
        print_msg('Password baru dan konfirmasi password baru tidak sama.');
    else {
        $db->query("UPDATE tb_admin SET pass='$pass2' WHERE user='$_SESSION[login]'");
        print_msg('Password berhasil diubah.', 'success');
    }
} elseif ($act == 'logout') {
    unset($_SESSION['login']);
    header("location:index.php");
}

/** jam */
elseif ($mod == 'jam_tambah') {
    $kode = $_POST['kode'];
    $nama = $_POST['nama'];

    if ($kode == '' || $nama == '')
        print_msg("Field bertanda * tidak boleh kosong!");
    elseif ($db->get_results("SELECT * FROM tb_jam WHERE kode_jam='$kode'"))
        print_msg("Kode sudah ada!");
    else {
        $db->query("INSERT INTO tb_jam (kode_jam, nama_jam) VALUES ('$kode', '$nama')");
        redirect_js("index.php?m=jam");
    }
} else if ($mod == 'jam_ubah') {
    $kode = $_POST['kode'];
    $nama = $_POST['nama'];

    if ($kode == '' || $nama == '')
        print_msg("Field bertanda * tidak boleh kosong!");
    else {
        $db->query("UPDATE tb_jam SET nama_jam='$nama'WHERE kode_jam='$_GET[ID]'");
        redirect_js("index.php?m=jam");
    }
} else if ($act == 'jam_hapus') {
    $db->query("DELETE FROM tb_jam WHERE kode_jam='$_GET[ID]'");
    $db->query("DELETE FROM tb_waktu WHERE kode_jam='$_GET[ID]'");
    header("location:index.php?m=jam");
}

/** hari */
elseif ($mod == 'hari_tambah') {
    $kode = $_POST['kode'];
    $nama = $_POST['nama'];

    if ($kode == '' || $nama == '')
        print_msg("Field bertanda * tidak boleh kosong!");
    elseif ($db->get_results("SELECT * FROM tb_hari WHERE kode_hari='$kode'"))
        print_msg("Kode sudah ada!");
    else {
        $db->query("INSERT INTO tb_hari (kode_hari, nama_hari) VALUES ('$kode', '$nama')");
        redirect_js("index.php?m=hari");
    }
} else if ($mod == 'hari_ubah') {
    $kode = $_POST['kode'];
    $nama = $_POST['nama'];

    if ($kode == '' || $nama == '')
        print_msg("Field bertanda * tidak boleh kosong!");
    else {
        $db->query("UPDATE tb_hari SET nama_hari='$nama'WHERE kode_hari='$_GET[ID]'");
        redirect_js("index.php?m=hari");
    }
} else if ($act == 'hari_hapus') {
    $db->query("DELETE FROM tb_hari WHERE kode_hari='$_GET[ID]'");
    $db->query("DELETE FROM tb_waktu WHERE kode_hari='$_GET[ID]'");
    header("location:index.php?m=hari");
}

/** waktu */
elseif ($mod == 'waktu_tambah') {
    $kode_hari  = $_POST['kode_hari'];
    $kode_jam   = $_POST['kode_jam'];
    if ($kode_hari == '' || $kode_jam == '')
        print_msg("Field yang bertanda * tidak boleh kosong!");
    elseif ($db->get_row("SELECT * FROM tb_waktu WHERE kode_hari='$kode_hari' AND kode_jam='$kode_jam'"))
        print_msg("Kombinasi hari dan jam sudah ada!");
    else {
        $db->query("INSERT INTO tb_waktu (kode_hari, kode_jam) VALUES ('$kode_hari', '$kode_jam')");
        redirect_js("index.php?m=waktu");
    }
} else if ($mod == 'waktu_ubah') {
    $kode_hari  = $_POST['kode_hari'];
    $kode_jam   = $_POST['kode_jam'];
    if ($kode_hari == '' || $kode_jam == '')
        print_msg("Field yang bertanda * tidak boleh kosong!");
    elseif ($db->get_row("SELECT * FROM tb_waktu WHERE kode_hari='$kode_hari' AND kode_jam='$kode_jam' AND kode_waktu<>'$_GET[ID]'"))
        print_msg("Kombinasi hari dan jam sudah ada!");
    else {
        $db->query("UPDATE tb_waktu SET kode_hari='$kode_hari', kode_jam='$kode_jam' WHERE kode_waktu='$_GET[ID]'");
        redirect_js("index.php?m=waktu");
    }
} else if ($act == 'waktu_hapus') {
    $db->query("DELETE FROM tb_waktu WHERE kode_waktu='$_GET[ID]'");
    header("location:index.php?m=waktu");
}

/** produk */
elseif ($mod == 'produk_tambah') {
    $kode = $_POST['kode'];
    $nama = $_POST['nama'];
    $keterangan = $_POST['keterangan'];

    if ($kode == '' || $nama == '')
        print_msg("Field bertanda * tidak boleh kosong!");
    elseif ($db->get_results("SELECT * FROM tb_produk WHERE kode_produk='$kode'"))
        print_msg("Kode sudah ada!");
    else {
        $db->query("INSERT INTO tb_produk (kode_produk, nama_produk, keterangan) VALUES ('$kode', '$nama', '$keterangan')");
        redirect_js("index.php?m=produk");
    }
} else if ($mod == 'produk_ubah') {
    $kode = $_POST['kode'];
    $nama = $_POST['nama'];
    $keterangan = $_POST['keterangan'];

    if ($kode == '' || $nama == '')
        print_msg("Field bertanda * tidak boleh kosong!");
    else {
        $db->query("UPDATE tb_produk SET nama_produk='$nama', keterangan='$keterangan' WHERE kode_produk='$_GET[ID]'");
        redirect_js("index.php?m=produk");
    }
} else if ($act == 'produk_hapus') {
    $db->query("DELETE FROM tb_produk WHERE kode_produk='$_GET[ID]'");
    header("location:index.php?m=produk");
}

/** teknisi */
else if ($mod == 'teknisi_tambah') {
    $kode = $_POST['kode'];
    $nama = $_POST['nama'];
    $keterangan = $_POST['keterangan'];

    if ($kode == '' || $nama == '')
        print_msg("Field bertanda * tidak boleh kosong!");
    elseif ($db->get_results("SELECT * FROM tb_teknisi WHERE kode_teknisi='$kode'"))
        print_msg("Kode sudah ada!");
    else {
        $db->query("INSERT INTO tb_teknisi (kode_teknisi, nama_teknisi, keterangan) VALUES ('$kode', '$nama', '$keterangan')");
        redirect_js("index.php?m=teknisi");
    }
} else if ($mod == 'teknisi_ubah') {
    $kode = $_POST['kode'];
    $nama = $_POST['nama'];
    $keterangan = $_POST['keterangan'];

    if ($kode == '' || $nama == '')
        print_msg("Field bertanda * tidak boleh kosong!");
    else {
        $db->query("UPDATE tb_teknisi SET nama_teknisi='$nama', keterangan='$keterangan' WHERE kode_teknisi='$_GET[ID]'");
        redirect_js("index.php?m=teknisi");
    }
} else if ($act == 'teknisi_hapus') {
    $db->query("DELETE FROM tb_teknisi WHERE kode_teknisi='$_GET[ID]'");
    $db->query("DELETE FROM tb_maintenance WHERE kode_teknisi='$_GET[ID]'");
    header("location:index.php?m=teknisi");
}

/** kelas */
elseif ($mod == 'kelas_tambah') {
    $kode  = $_POST['kode'];
    $nama   = $_POST['nama'];
    $keterangan = $_POST['keterangan'];
    if ($kode == '' || $nama == '')
        print_msg("Field yang bertanda * tidak boleh kosong!");
    elseif ($db->get_row("SELECT * FROM tb_kelas WHERE kode_kelas='$kode'"))
        print_msg("Kode kelas sudah ada!");
    else {
        $db->query("INSERT INTO tb_kelas (kode_kelas, nama_kelas, keterangan) VALUES ('$kode', '$nama', '$keterangan')");
        redirect_js("index.php?m=kelas");
    }
} else if ($mod == 'kelas_ubah') {
    $kode  = $_POST['kode'];
    $nama   = $_POST['nama'];
    $keterangan = $_POST['keterangan'];
    if ($kode == '' || $nama == '')
        print_msg("Field yang bertanda * tidak boleh kosong!");
    elseif ($db->get_row("SELECT * FROM tb_kelas WHERE kode_kelas='$kode' AND kode_kelas<>'$_GET[ID]'"))
        print_msg("Kode kelas sudah ada!");
    else {
        $db->query("UPDATE tb_kelas SET kode_kelas='$kode', nama_kelas='$nama', keterangan='$keterangan' WHERE kode_kelas='$_GET[ID]'");
        redirect_js("index.php?m=kelas");
    }
} else if ($act == 'kelas_hapus') {
    $db->query("DELETE FROM tb_kelas WHERE kode_kelas='$_GET[ID]'");
    $db->query("DELETE FROM tb_maintenance WHERE kode_kelas='$_GET[ID]'");
    header("location:index.php?m=kelas");
}

/** pelanggan */
elseif ($mod == 'pelanggan_tambah') {
    $kode = $_POST['kode'];
    $nama = $_POST['nama'];
    $sks = $_POST['sks'];

    if ($kode == '' || $nama == '')
        print_msg("Field bertanda * tidak boleh kosong!");
    elseif ($db->get_results("SELECT * FROM tb_pelanggan WHERE kode_pelanggan='$kode'"))
        print_msg("Kode sudah ada!");
    else {
        $db->query("INSERT INTO tb_pelanggan (kode_pelanggan, nama_pelanggan, sks) VALUES ('$kode', '$nama', '$sks')");
        redirect_js("index.php?m=pelanggan");
    }
} else if ($mod == 'pelanggan_ubah') {
    $kode = $_POST['kode'];
    $nama = $_POST['nama'];
    $sks = $_POST['sks'];

    if ($kode == '' || $nama == '')
        print_msg("Field bertanda * tidak boleh kosong!");
    else {
        $db->query("UPDATE tb_pelanggan SET nama_pelanggan='$nama', sks='$sks' WHERE kode_pelanggan='$_GET[ID]'");
        redirect_js("index.php?m=pelanggan");
    }
} else if ($act == 'pelanggan_hapus') {
    $db->query("DELETE FROM tb_pelanggan WHERE kode_pelanggan='$_GET[ID]'");
    $db->query("DELETE FROM tb_maintenance WHERE kode_pelanggan='$_GET[ID]'");
    header("location:index.php?m=pelanggan");
}

/** maintenance */
elseif ($mod == 'maintenance_tambah') {
    $kode_pelanggan  = $_POST['kode_pelanggan'];
    $kode_kelas   = $_POST['kode_kelas'];
    $kode_teknisi   = $_POST['kode_teknisi'];
    if ($kode_pelanggan == '' || $kode_kelas == '' || $kode_teknisi == '')
        print_msg("Field yang bertanda * tidak boleh kosong!");
    elseif ($db->get_row("SELECT * FROM tb_maintenance WHERE kode_pelanggan='$kode_pelanggan' AND kode_kelas='$kode_kelas' AND kode_teknisi='$kode_teknisi'"))
        print_msg("Kombinasi sudah ada!");
    else {
        $db->query("INSERT INTO tb_maintenance (kode_pelanggan, kode_kelas, kode_teknisi) VALUES ('$kode_pelanggan', '$kode_kelas', '$kode_teknisi')");
        redirect_js("index.php?m=maintenance");
    }
} else if ($mod == 'maintenance_ubah') {
    $kode_pelanggan  = $_POST['kode_pelanggan'];
    $kode_kelas   = $_POST['kode_kelas'];
    $kode_teknisi   = $_POST['kode_teknisi'];
    if ($kode_pelanggan == '' || $kode_kelas == '' || $kode_teknisi == '')
        print_msg("Field yang bertanda * tidak boleh kosong!");
    elseif ($db->get_row("SELECT * FROM tb_maintenance WHERE kode_pelanggan='$kode_pelanggan' AND kode_kelas='$kode_kelas' AND kode_teknisi='$kode_teknisi' AND kode_maintenance<>'$_GET[ID]'"))
        print_msg("Kombinasi sudah ada!");
    else {
        $db->query("UPDATE tb_maintenance SET kode_pelanggan='$kode_pelanggan', kode_kelas='$kode_kelas', kode_teknisi='$kode_teknisi' WHERE kode_maintenance='$_GET[ID]'");
        redirect_js("index.php?m=maintenance");
    }
} else if ($act == 'maintenance_hapus') {
    $db->query("DELETE FROM tb_maintenance WHERE kode_maintenance='$_GET[ID]'");
    header("location:index.php?m=maintenance");
}
