<div class="page-header">
    <h1>Jadwal Preventif Maintenance</h1>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <form class="form-inline">
            <input type="hidden" name="m" value="hasil" />
            <div class="form-group">
                <input class="form-control" type="text" placeholder="Pencarian. . ." name="q" value="<?= $_GET['q'] ?>" />
            </div>
            <div class="form-group">
                <button class="btn btn-success"><span class="glyphicon glyphicon-refresh"></span> Refresh</button>
            </div>
            <div class="form-group">
                <a class="btn btn-default" href="cetak.php?m=hasil&q=<?= $_GET['q'] ?>" target="_blank"><span class="glyphicon glyphicon-print"></span> Cetak</a>
            </div>
        </form>
    </div>

    <table class="table table-bordered table-hover table-striped">
        <thead>
            <tr class="nw">
                <th>No</th>
                <th>Hari</th>
                <th>Jam</th>
                <th>Nama pelanggan</th>
                <th>Produk</th>
                <th>Nama Teknisi</th>
             
            </tr>
        </thead>
        <?php
        $q = esc_field($_GET['q']);
        $rows = $db->get_results("SELECT h.nama_hari, tb_jam.nama_jam, m.nama_pelanggan, m.sks, k.kode_kelas, d.nama_teknisi, r.nama_produk, tb_jam.nama_jam + INTERVAL m.sks * 45 MINUTE AS jam_selesai
    FROM tb_maintenance k 
    	INNER JOIN tb_teknisi d ON d.kode_teknisi=k.kode_teknisi
    	INNER JOIN tb_pelanggan m ON m.kode_pelanggan=k.kode_pelanggan
    	INNER JOIN tb_jadwal j ON j.maintenance = k.kode_maintenance
    	INNER JOIN tb_produk r ON r.kode_produk = j.produk
    	INNER JOIN tb_waktu w ON w.kode_waktu = j.waktu
    	INNER JOIN tb_hari h ON h.kode_hari = w.kode_hari
    	INNER JOIN tb_jam ON tb_jam.kode_jam = w.kode_jam
    WHERE nama_hari LIKE '%$q' 
        OR nama_pelanggan LIKE '%$q'
        OR kode_kelas LIKE '%$q'
        OR nama_produk LIKE '%$q'
        OR nama_teknisi LIKE '%$q'
    ORDER BY w.kode_hari, w.kode_jam");
        $no = 0;

        foreach ($rows as $row) : ?>
            <tr>
                <td><?= ++$no ?></td>
                <td><?= $row->nama_hari ?></td>
                <td><?= substr($row->nama_jam, 0, 5) ?></td>
                <td><?= $row->nama_pelanggan ?></td>
                <td><?= $row->nama_produk ?></td>
                <td><?= $row->nama_teknisi ?></td>
              
            </tr>
        <?php endforeach; ?>
    </table>
</div>