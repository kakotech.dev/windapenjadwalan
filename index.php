<?php
include 'functions.php';
// if (!$_SESSION['login'])
//   header('location:login.php');
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <title>Penjadwalan PREVENTIF MAINTENANCE</title>
  <link href="favicon.ico" rel="icon" />
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">

  <link rel="stylesheet" href="assets/css/AdminLTE.min.css">
  <link rel="stylesheet" href="assets/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <link href="assets/css/select2.min.css" rel="stylesheet" />

  <script src="assets/jquery/jquery.min.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/js/highcharts.js"></script>
  <script src="assets/js/highcharts-3d.js"></script>
  <script src="assets/js/exporting.js"></script>
  <script src="assets/js/adminlte.min.js"></script>
  <script>
    $(document).ready(function() {
      $('.sidebar-menu').tree()
    })
  </script>
  <script src="assets/js/select2.min.js"></script>
  <script type="text/javascript">
    $(function() {
      $("select:not(.default)").select2();
    })
  </script>
  <style type="text/css">
    .aw {
      width: auto;
    }

    .nw {
      white-space: nowrap;
    }

    .page-header {
      margin: 0 0 10px 0;
    }

    .page-header h1 {
      margin: 0;
    }
  </style>
</head>

<body class="hold-transition skin-red sidebar-mini ">
  <div class="wrapper">
    <header class="main-header">
      <a href="?" class="logo">
        <span class="logo-mini">.</span>
        <span class="logo-lg">.</span>
      </a>
      <nav class="navbar navbar-static-top">
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>
        <ul class="nav navbar-nav">

        </ul>
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <?php if ($_SESSION['user']) : ?>
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="assets/img/avatar.png" class="user-image" alt="User Image">
                  <span class="hidden-xs"><?= ucfirst($_SESSION['user']->nama_user) ?></span>
                </a>
                <ul class="dropdown-menu">
                  <li class="user-header">
                    <img src="assets/img/avatar.png" class="img-circle" alt="User Image">
                  </li>
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="?m=password" class="btn btn-default btn-flat">Password</a>
                    </div>
                    <div class="pull-right">
                      <a href="aksi.php?act=logout" class="btn btn-default btn-flat">Logout</a>
                    </div>
                  </li>
                <?php else : ?>
                  <li><a href="login.php">Login</a></li>
                <?php endif ?>
                </ul>
              </li>
              <li class="hidden">
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li>
          </ul>
        </div>
      </nav>
    </header>
    <aside class="main-sidebar">
      <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
          <li class="header">MENU UTAMA</li>
          <li><a href="?m=home"><i class="fa fa-dashboard"></i> <span>Beranda</span></a></li>
          <?php if ($_SESSION['login']) : ?>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-clock-o"></i>
                <span>Waktu</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="?m=waktu"><i class="fa fa-circle-o"></i> Data Waktu</a></li>
                <li><a href="?m=hari"><i class="fa fa-circle-o"></i> Hari</a></li>
                <li><a href="?m=jam"><i class="fa fa-circle-o"></i> Jam</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-clock-o"></i>
                <span>Maintenance</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="?m=maintenance"><i class="fa fa-circle-o"></i> Data Maintenance</a></li>
                <li><a href="?m=pelanggan"><i class="fa fa-circle-o"></i> Pelanggan</a></li>
                <li><a href="?m=teknisi"><i class="fa fa-circle-o"></i> Teknisi</a></li>

              </ul>
            </li>
            <li><a href="?m=produk"><i class="fa fa-th"></i> <span>Produk</span></a></li>
            <li><a href="?m=penjadwalan"><i class="fa fa-twitter"></i> <span>Penjadwalan</span></a></li>
            <li><a href="?m=hasil"><i class="fa fa-calendar-check-o"></i> <span>Hasil</span></a></li>
            <li><a href="?m=password"><i class="fa fa-lock"></i> <span>Password</span></a></li>
            <li><a href="aksi.php?act=logout"><i class="fa fa-signal"></i> <span>Logout</span></a></li>
          <?php else : ?>
            <li><a href="?m=hasil"><i class="fa fa-star"></i> Hasil Jadwal</a></li>
            <li><a href="login.php"><i class="fa fa-sign-in"></i> Login</a></li>
          <?php endif ?>
        </ul>
      </section>
    </aside>
    <div class="content-wrapper">
      <section class="content">
        <?php
        if (file_exists($mod . '.php'))
          include $mod . '.php';
        else
          include 'home.php';
        ?>
      </section>
    </div>
    <footer class="main-footer">
      Copyright &copy;
      <div class="pull-right hidden-xs">

      </div>
    </footer>
    <aside class="control-sidebar control-sidebar-dark">
    </aside>
    <div class="control-sidebar-bg"></div>
  </div>
</body>

</html>