<?php
error_reporting(~E_NOTICE);
session_start();

ini_set('max_execution_time', 60 * 3);
ini_set('memory_limit', '128M');

include 'config.php';
include 'includes/db.php';
$db = new DB($config['server'], $config['username'], $config['password'], $config['database_name']);
include 'includes/general.php';
include 'includes/paging.php';

$mod = $_GET['m'];
$act = $_GET['act'];

function AG_get_hari_option($selected = '')
{
    global $db;
    $rows = $db->get_results("SELECT kode_hari, nama_hari FROM tb_hari ORDER BY kode_hari");
    foreach ($rows as $row) {
        if ($row->kode_hari == $selected)
            $a .= "<option value='$row->kode_hari' selected>[$row->kode_hari] $row->nama_hari</option>";
        else
            $a .= "<option value='$row->kode_hari'>[$row->kode_hari] $row->nama_hari</option>";
    }
    return $a;
}

function AG_get_jam_option($selected = '')
{
    global $db;
    $rows = $db->get_results("SELECT kode_jam, nama_jam FROM tb_jam ORDER BY kode_jam");
    foreach ($rows as $row) {
        if ($row->kode_jam == $selected)
            $a .= "<option value='$row->kode_jam' selected>[$row->kode_jam] $row->nama_jam</option>";
        else
            $a .= "<option value='$row->kode_jam'>[$row->kode_jam] $row->nama_jam</option>";
    }
    return $a;
}

function AG_get_pelanggan_option($selected = '')
{
    global $db;
    $rows = $db->get_results("SELECT kode_pelanggan, nama_pelanggan FROM tb_pelanggan ORDER BY kode_pelanggan");
    foreach ($rows as $row) {
        if ($row->kode_pelanggan == $selected)
            $a .= "<option value='$row->kode_pelanggan' selected>[$row->kode_pelanggan] $row->nama_pelanggan</option>";
        else
            $a .= "<option value='$row->kode_pelanggan'>[$row->kode_pelanggan] $row->nama_pelanggan</option>";
    }
    return $a;
}

function AG_get_kelas_option($selected = '')
{
    global $db;
    $rows = $db->get_results("SELECT kode_kelas, nama_kelas FROM tb_kelas ORDER BY kode_kelas");
    foreach ($rows as $row) {
        if ($row->kode_kelas == $selected)
            $a .= "<option value='$row->kode_kelas' selected>[$row->kode_kelas] $row->nama_kelas</option>";
        else
            $a .= "<option value='$row->kode_kelas'>[$row->kode_kelas] $row->nama_kelas</option>";
    }
    return $a;
}

function AG_get_teknisi_option($selected = '')
{
    global $db;
    $rows = $db->get_results("SELECT kode_teknisi, nama_teknisi FROM tb_teknisi ORDER BY kode_teknisi");
    foreach ($rows as $row) {
        if ($row->kode_teknisi == $selected)
            $a .= "<option value='$row->kode_teknisi' selected>[$row->kode_teknisi] $row->nama_teknisi</option>";
        else
            $a .= "<option value='$row->kode_teknisi'>[$row->kode_teknisi] $row->nama_teknisi</option>";
    }
    return $a;
}
