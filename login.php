<?php
include 'functions.php';
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <title>Login</title>
  <link href="favicon.ico" rel="icon" />
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="assets/css/AdminLTE.min.css">
  <link rel="stylesheet" href="assets/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <script src="assets/jquery/jquery.min.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/js/adminlte.min.js"></script>
</head>

<body class="hold-transition login-page">
  <div class="login-box" style="width: 400px">
    <div class="login-logo">
      <h1>Silahkan Masuk</h1>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
      <form action="?act=login" method="post">
        <?php if ($_POST) include 'aksi.php' ?>
        <div class="form-group has-feedback">
          <input type="text" class="form-control" placeholder="Username" name="user">
          <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
          <input type="password" class="form-control" placeholder="Password" name="pass">
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">
          <div class="col-xs-8">
            <a class="btn btn-danger hidden" href="index.php"><span class="glyphicon glyphicon-arrow-left"></span> Kembali</a>
          </div>
          <div class="col-xs-4">
            <button type="submit" class="btn btn-primary btn-block">Masuk</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</body>

</html>