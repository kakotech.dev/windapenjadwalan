<div class="page-header">
    <h1>Penjadwalan</h1>
</div>
<?php
$success = true;
?>
<div class="row">
    <div class="col-md-6">
        <form action="?">
            <input type="hidden" name="m" value="penjadwalan" />
            <input type="hidden" name="quantum" value="1" />
            <button class="btn btn-primary">Generate Jadwal</button>
            <!-- Menampilkan tombol jadwal setelah request generate -->
            <?php if ($success && isset($_GET['quantum'])) : ?>
                <a class="btn btn-success" href="?m=hasil" target="_blank">Lihat Jadwal</a>
            <?php endif ?>
        </form>
    </div>
</div>
<?php
include 'roundrobin.php';

if ($success && isset($_GET['quantum'])) {
    echo '<hr />';
    /* Memanggil Fungsi Run/generate roundrobin */
    $roundrobin = new RoundRobin();
    $roundrobin->run();
    print_msg('Generate berhasil!', 'success');
}
?>