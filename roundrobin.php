<?php
class RoundRobin
{

    function run()
    {
        global $db;
        /* Mengambil data waktu, maintenance, produk dari database */
        $waktu_list = $db->get_results("SELECT * FROM tb_waktu");
        $maintenance_list = $db->get_results("SELECT * FROM tb_maintenance");
        $produk_list = $db->get_results("SELECT * FROM tb_produk");
        $jadwal_list = array();

        /* Inisiasi counter untuk pelanggan, teknisi, produk. Urutan ini merupakan prioritas dalam menyusun jadwal */
        $pelanggan_counter = array();
        $teknisi_counter = array();
        $produk_counter = array();
        foreach ($maintenance_list as $row) {
            $pelanggan_counter[$row->kode_pelanggan] = 0;
            $teknisi_counter[$row->kode_teknisi] = 0;
        }
        foreach ($produk_list as $row) {
            $produk_counter[$row->kode_produk] = 0;
        }

        /* Mengisi jadwal untuk daftar waktu */
        foreach ($waktu_list as $waktu) {
            if (count($maintenance_list) == 0) {
                break;
            }
            $jadwal = array();
            /* Mengurutkan antrian pelanggan, teknisi, produk */
            ksort($pelanggan_counter);
            asort($pelanggan_counter);
            ksort($teknisi_counter);
            asort($teknisi_counter);
            ksort($produk_counter);
            asort($produk_counter);
            foreach ($produk_counter as $key => $value) {
                $produk = $key;
                $produk_counter[$key]++;
                break;
            }
            /* Menyusun jadwal untuk pelanggan, teknisi, dan produk */
            foreach ($pelanggan_counter as $pelanggan => $pelanggan_count) {
                foreach ($teknisi_counter as $teknisi => $teknisi_count) {
                    foreach ($maintenance_list as $key => $maintenance) {
                        /* Mengecek ketersedian pasangan (pelanggan, teknisi) dalam daftar maintenance */
                        if ($maintenance->kode_pelanggan == $pelanggan && $maintenance->kode_teknisi == $teknisi) {
                            /* Mengisi jadwal */
                            $jadwal = array(
                                'maintenance' => $maintenance->kode_maintenance,
                                'produk' => $produk,
                                'waktu' => $waktu->kode_waktu
                            );
                            array_push($jadwal_list, $jadwal);
                            /* Counter pelanggan, teknisi ditambah 1 */
                            $pelanggan_counter[$pelanggan]++;
                            $teknisi_counter[$teknisi]++;
                            /* Data dikeluarkan dari daftar antrian maintenance */
                            unset($maintenance_list[$key]);
                            break;
                        }
                    }
                    if (count($jadwal) > 0) {
                        break;
                    }
                }
                if (count($jadwal) > 0) {
                    break;
                }
            }
        }

        /* Menyimpan data jadwal ke database */
        $db->query("TRUNCATE tb_jadwal");
        foreach ($jadwal_list as $jadwal) {
            $db->query("INSERT INTO tb_jadwal(maintenance, produk, waktu) VALUES (
                '" . $jadwal['maintenance'] . "', 
                '" . $jadwal['produk'] . "', 
                '" . $jadwal['waktu'] . "')");
        }


    }

    
}
