-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 09, 2020 at 08:24 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ag_custom`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_admin`
--

CREATE TABLE `tb_admin` (
  `user` varchar(16) NOT NULL,
  `pass` varchar(16) NOT NULL,
  `level` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_admin`
--

INSERT INTO `tb_admin` (`user`, `pass`, `level`) VALUES
('admin', 'admin', 'admin'),
('user', 'user', 'user');

-- --------------------------------------------------------

--
-- Table structure for table `tb_hari`
--

CREATE TABLE `tb_hari` (
  `kode_hari` varchar(256) NOT NULL,
  `nama_hari` varchar(256) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_hari`
--

INSERT INTO `tb_hari` (`kode_hari`, `nama_hari`) VALUES
('1', 'Senin'),
('2', 'Selasa'),
('3', 'Rabu'),
('4', 'Kamis'),
('5', 'Jumat'),
('6', 'Sabtu'),
('7', 'Minggu');

-- --------------------------------------------------------

--
-- Table structure for table `tb_jadwal`
--

CREATE TABLE `tb_jadwal` (
  `maintenance` int(11) NOT NULL,
  `produk` int(11) NOT NULL,
  `waktu` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_jadwal`
--

INSERT INTO `tb_jadwal` (`maintenance`, `produk`, `waktu`) VALUES
(2, 1, 32),
(3, 3, 4),
(4, 4, 21),
(5, 2, 36),
(6, 5, 1),
(7, 2, 16),
(8, 3, 24),
(9, 1, 35),
(10, 3, 36),
(11, 5, 34),
(12, 3, 17),
(13, 3, 16),
(14, 1, 8),
(15, 1, 9),
(16, 1, 26),
(17, 3, 6),
(18, 2, 5),
(19, 5, 17),
(20, 3, 22),
(21, 5, 29),
(22, 3, 10),
(23, 4, 1),
(24, 4, 2),
(25, 5, 30),
(26, 2, 24),
(27, 3, 1),
(28, 1, 18),
(29, 1, 29),
(30, 2, 6);

-- --------------------------------------------------------

--
-- Table structure for table `tb_jam`
--

CREATE TABLE `tb_jam` (
  `kode_jam` varchar(256) NOT NULL,
  `nama_jam` time NOT NULL DEFAULT '00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_jam`
--

INSERT INTO `tb_jam` (`kode_jam`, `nama_jam`) VALUES
('01', '07:30:00'),
('02', '09:00:00'),
('03', '10:30:00'),
('04', '12:00:00'),
('05', '13:30:00'),
('06', '15:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_maintenance`
--

CREATE TABLE `tb_maintenance` (
  `kode_maintenance` int(11) NOT NULL,
  `kode_pelanggan` varchar(16) DEFAULT NULL,
  `kode_kelas` varchar(16) DEFAULT NULL,
  `kode_teknisi` varchar(16) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_maintenance`
--

INSERT INTO `tb_maintenance` (`kode_maintenance`, `kode_pelanggan`, `kode_kelas`, `kode_teknisi`) VALUES
(2, 'MK002', 'A', 'D001'),
(3, 'MK003', 'A', 'D001'),
(4, 'MK004', 'A', 'D002'),
(5, 'MK005', 'A', 'D002'),
(6, 'MK006', 'A', 'D002'),
(7, 'MK001', 'B', 'D003'),
(8, 'MK002', 'B', 'D003'),
(9, 'MK003', 'B', 'D003'),
(10, 'MK004', 'B', 'D004'),
(11, 'MK005', 'B', 'D004'),
(12, 'MK006', 'B', 'D004'),
(13, 'MK001', 'C', 'D005'),
(14, 'MK002', 'C', 'D005'),
(15, 'MK003', 'C', 'D005'),
(16, 'MK004', 'C', 'D006'),
(17, 'MK005', 'C', 'D006'),
(18, 'MK006', 'C', 'D006'),
(19, 'MK001', 'D', 'D007'),
(20, 'MK002', 'D', 'D007'),
(21, 'MK003', 'D', 'D007'),
(22, 'MK004', 'D', 'D008'),
(23, 'MK005', 'D', 'D008'),
(24, 'MK006', 'D', 'D008'),
(25, 'MK001', 'E', 'D009'),
(26, 'MK002', 'E', 'D009'),
(27, 'MK003', 'E', 'D009'),
(28, 'MK004', 'E', 'D010'),
(29, 'MK005', 'E', 'D010'),
(30, 'MK005', 'E', 'D010');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pelanggan`
--

CREATE TABLE `tb_pelanggan` (
  `kode_pelanggan` varchar(256) NOT NULL,
  `nama_pelanggan` varchar(256) DEFAULT NULL,
  `sks` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pelanggan`
--

INSERT INTO `tb_pelanggan` (`kode_pelanggan`, `nama_pelanggan`, `sks`) VALUES
('MK001', 'Bank Sumut', 0),
('MK002', 'Bank BRI', 0),
('MK003', 'Sinarmas', 0),
('MK004', 'BPPD SUMUT', 0),
('MK005', 'BMKG Sumut', 0),
('MK006', 'DISPENDA SUMUT', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_produk`
--

CREATE TABLE `tb_produk` (
  `kode_produk` int(11) NOT NULL,
  `nama_produk` varchar(256) NOT NULL,
  `keterangan` varchar(256) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_produk`
--

INSERT INTO `tb_produk` (`kode_produk`, `nama_produk`, `keterangan`) VALUES
(1, 'Vsat Voice', ''),
(2, 'Mvsat', ''),
(3, 'Vsat Internet', ''),
(4, 'Vsat Ku-Band', ''),
(5, 'Vsat Data', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_teknisi`
--

CREATE TABLE `tb_teknisi` (
  `kode_teknisi` varchar(16) NOT NULL,
  `nama_teknisi` varchar(255) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_teknisi`
--

INSERT INTO `tb_teknisi` (`kode_teknisi`, `nama_teknisi`, `keterangan`) VALUES
('D001', 'Ferdiansah', ''),
('D002', 'Wanda Kurniawan', ''),
('D003', 'waldianto', ''),
('D004', 'Muhammad irvan', ''),
('D005', 'marshall', ''),
('D006', 'raja baruna', ''),
('D007', 'sudiarjono', ''),
('D008', 'Dosen 08', ''),
('D009', 'arfencis', ''),
('D010', 'fahri', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_waktu`
--

CREATE TABLE `tb_waktu` (
  `kode_waktu` int(11) NOT NULL,
  `kode_hari` varchar(256) NOT NULL DEFAULT '0',
  `kode_jam` varchar(256) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_waktu`
--

INSERT INTO `tb_waktu` (`kode_waktu`, `kode_hari`, `kode_jam`) VALUES
(1, '1', '01'),
(2, '1', '02'),
(3, '1', '03'),
(4, '1', '04'),
(5, '1', '05'),
(6, '2', '02'),
(7, '2', '01'),
(8, '2', '03'),
(9, '2', '04'),
(10, '2', '05'),
(11, '3', '01'),
(12, '3', '02'),
(13, '3', '03'),
(14, '3', '04'),
(15, '3', '05'),
(16, '4', '01'),
(17, '4', '02'),
(18, '4', '03'),
(19, '4', '04'),
(20, '4', '05'),
(21, '5', '01'),
(22, '5', '02'),
(23, '5', '03'),
(24, '5', '04'),
(25, '5', '05'),
(26, '6', '01'),
(27, '6', '02'),
(28, '6', '03'),
(29, '6', '04'),
(30, '6', '05'),
(31, '1', '06'),
(32, '2', '06'),
(33, '3', '06'),
(34, '4', '06'),
(35, '5', '06'),
(36, '6', '06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`user`);

--
-- Indexes for table `tb_hari`
--
ALTER TABLE `tb_hari`
  ADD PRIMARY KEY (`kode_hari`);

--
-- Indexes for table `tb_jam`
--
ALTER TABLE `tb_jam`
  ADD PRIMARY KEY (`kode_jam`);

--
-- Indexes for table `tb_maintenance`
--
ALTER TABLE `tb_maintenance`
  ADD PRIMARY KEY (`kode_maintenance`);

--
-- Indexes for table `tb_pelanggan`
--
ALTER TABLE `tb_pelanggan`
  ADD PRIMARY KEY (`kode_pelanggan`);

--
-- Indexes for table `tb_produk`
--
ALTER TABLE `tb_produk`
  ADD PRIMARY KEY (`kode_produk`);

--
-- Indexes for table `tb_teknisi`
--
ALTER TABLE `tb_teknisi`
  ADD PRIMARY KEY (`kode_teknisi`);

--
-- Indexes for table `tb_waktu`
--
ALTER TABLE `tb_waktu`
  ADD PRIMARY KEY (`kode_waktu`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_maintenance`
--
ALTER TABLE `tb_maintenance`
  MODIFY `kode_maintenance` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `tb_produk`
--
ALTER TABLE `tb_produk`
  MODIFY `kode_produk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_waktu`
--
ALTER TABLE `tb_waktu`
  MODIFY `kode_waktu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
