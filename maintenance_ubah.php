<?php
$row = $db->get_row("SELECT * FROM tb_maintenance WHERE kode_maintenance='$_GET[ID]'");
?>
<div class="page-header">
    <h1>Ubah Data Maintenance</h1>
</div>
<div class="row">
    <div class="col-sm-6">
        <?php if ($_POST) include 'aksi.php' ?>
        <form method="post">
            <div class="form-group">
                <label>Data P <span class="text-danger">*</span></label>
                <select class="form-control" name="kode_pelanggan">
                    <option value=""></option>
                    <?= AG_get_pelanggan_option($row->kode_pelanggan) ?>
                </select>
            </div>
    </div>
    <div class="form-group">
        <label>Kelas <span class="text-danger">*</span></label>
        <select class="form-control" name="kode_kelas">
            <option value=""></option>
            <?= AG_get_kelas_option($_POST['kode_kelas']) ?>
        </select>
    </div>
    <div class="form-group">
        <label>Kelas <span class="text-danger">*</span></label>
        <select class="form-control" name="kode_kelas">
            <option value=""></option>
            <?= AG_get_kelas_option($row->kode_kelas) ?>
        </select>
    </div>
    <div class="form-group">
        <label>Teknisi <span class="text-danger">*</span></label>
        <select class="form-control" name="kode_teknisi">
            <option value=""></option>
            <?= AG_get_teknisi_option($row->kode_teknisi) ?>
        </select>
    </div>
    <div class="form-group">
        <button class="btn btn-primary"><span class="glyphicon glyphicon-save"></span> Simpan</button>
        <a class="btn btn-danger" href="?m=maintenance"><span class="glyphicon glyphicon-arrow-left"></span> Kembali</a>
    </div>
    </form>
</div>
</div>